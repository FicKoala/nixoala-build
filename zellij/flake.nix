{
  description = "Zellij terminal multiplexer";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/master";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    zellij-src = {
      url = "github:zellij-org/zellij/main";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, fenix, zellij-src }:
    let
      supportedSystems = [ "x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin" ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      nixpkgsFor = forAllSystems (system: import nixpkgs {
        inherit system;
        overlays = [ fenix.overlays.default ];
      });
    in
    {
      packages = forAllSystems (system:
        let
          pkgs = nixpkgsFor.${system};
          rustToolchain = pkgs.fenix.stable.toolchain;
          isDarwin = pkgs.stdenv.isDarwin;
        in
        {
          default = pkgs.rustPlatform.buildRustPackage {
            pname = "zellij";
            version = "main";
            src = zellij-src;

            cargoLock = {
              lockFile = "${zellij-src}/Cargo.lock";
            };

            nativeBuildInputs = with pkgs; [ 
              pkg-config
              perl
              cmake
              rustToolchain
            ] ++ pkgs.lib.optionals isDarwin [
              libiconv
            ];

            buildInputs = with pkgs; [
              openssl.dev
              curl.dev
            ] ++ pkgs.lib.optionals isDarwin [
              libiconv
              darwin.apple_sdk.frameworks.Security
              darwin.apple_sdk.frameworks.SystemConfiguration
              darwin.apple_sdk.frameworks.CoreFoundation
              darwin.apple_sdk.frameworks.CoreServices
              darwin.apple_sdk.frameworks.Foundation
              darwin.libobjc
              darwin.apple_sdk.frameworks.AppKit
            ];

            OPENSSL_NO_VENDOR = 1;
            OPENSSL_DIR = "${pkgs.openssl.dev}";
            OPENSSL_LIB_DIR = "${pkgs.openssl.out}/lib";

            LIBCLANG_PATH = "${pkgs.libclang.lib}/lib";

            preBuild = pkgs.lib.optionalString isDarwin ''
              export MACOSX_DEPLOYMENT_TARGET=10.13
            '';

            NIX_LDFLAGS = pkgs.lib.optionalString isDarwin ''
              -F${pkgs.darwin.apple_sdk.frameworks.Foundation}/Library/Frameworks
              -F${pkgs.darwin.apple_sdk.frameworks.AppKit}/Library/Frameworks
              -framework Foundation
              -framework AppKit
            '';

            meta = with pkgs.lib; {
              description = "A terminal workspace with batteries included";
              homepage = "https://zellij.dev";
              license = licenses.mit;
              maintainers = with maintainers; [ therealansh ];
            };
          };
        }
      );
    };
}
