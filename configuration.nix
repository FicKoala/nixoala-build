# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ pkgs, lib, ... }:
 {

  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];
  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/vda"; # or "nodev" for efi only

  nix.settings.experimental-features = ["nix-command" "flakes"];
  networking.hostName = "nixoala"; # Define your hostname

  system = {
    autoUpgrade = {
      enable = true;
      allowReboot = true; 
      flake = "/etc/nixos#nixoala";
      dates = "04:00";
      flags = [
      "--update-input" "pkgs" "--recreate-lock-file"
      ];
    };
 };

  # Set your time zone.
   time.timeZone = "Australia/Melbourne";


  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.nixoala = {
    isNormalUser = true;
    extraGroups = [ "wheel" "input" ]; # Enable ‘sudo’ for the user.
    openssh.authorizedKeys.keys = [
      (builtins.readFile ./key.pub)
    ];
    initialPassword = "changeMePassword";
    hashedPassword = "$6$G3XnjSl..VhSKwVv$ofuE3n6E7qMHiRmveT0UQQ6tZtk4zyJo0k/61Kda.Y5QD8WZAx6F0gmOCgCVVXH8JlSwymJZpQ.dB.KXR6dWH/";
    shell = pkgs.zsh;
  };

  programs = {
    zsh.enable = true;
    ssh.startAgent = true;
    mosh.enable = true;
    starship.enable = true;
  };

    environment.systemPackages = (with pkgs; [ 
      neovim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
      nushell 
      zoxide 
      starship 
      gh
      neovim
      git
      wget2
      zsh
      pijul
      patchelfUnstable
      xstow
      unzip
      rm-improved 
      openssl
      curl
      nil
      jujutsu
      stow
      xclip
      difftastic
      minicom
    ]);

    environment.shellInit = ''
      if command -v zoxide &> /dev/null; then eval "$(zoxide init zsh)"
      fi
      if command -v starship &> /dev/null; then eval "$(starship init zsh)"
      fi
      '';


  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = "no";
  services.openssh.settings.PasswordAuthentication = false;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ 3000..3030 ] ++
  # [ 8000..8080 ];
  networking.firewall = {
    enable = true;
    allowedTCPPortRanges = [
      { from = 3000; to = 3030; }
      { from = 8000; to = 8100; }
      { from = 33000; to = 33100;}
    ];
    allowedUDPPortRanges = [
      { from = 8000; to = 8100; }
    ];
  };

  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;
  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.11"; # Did you read the comment?
}
