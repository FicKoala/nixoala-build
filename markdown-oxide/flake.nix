{
  description = "A markdown language server with Obsidian syntax support";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/master";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    markdown-oxide-src = {
      url = "github:Feel-ix-343/markdown-oxide/main";
      flake = false;
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      fenix,
      markdown-oxide-src,
    }:
    let
      supportedSystems = [
        "x86_64-linux"
        "aarch64-linux"
        "x86_64-darwin"
        "aarch64-darwin"
      ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      nixpkgsFor = forAllSystems (
        system:
        import nixpkgs {
          inherit system;
          overlays = [ fenix.overlays.default ];
        }
      );
    in
    {
      packages = forAllSystems (
        system:
        let
          pkgs = nixpkgsFor.${system};
          rustToolchain = pkgs.fenix.stable.toolchain;
          isDarwin = pkgs.stdenv.isDarwin;
        in
        {
          default = pkgs.rustPlatform.buildRustPackage {
            pname = "markdown-oxide";
            version = "main";
            src = markdown-oxide-src;

            cargoLock = {
              lockFile = "${markdown-oxide-src}/Cargo.lock";
              outputHashes = {
                "tower-lsp-0.20.0" = "sha256-QRP1LpyI52KyvVfbBG95LMpmI8St1cgf781v3oyC3S4=";
              };
            };

            nativeBuildInputs = with pkgs; [
              pkg-config
              rustToolchain
            ];

            buildInputs =
              with pkgs;
              [
                openssl.dev
              ]
              ++ pkgs.lib.optionals isDarwin [
                darwin.apple_sdk.frameworks.Security
                darwin.apple_sdk.frameworks.SystemConfiguration
              ];

            OPENSSL_NO_VENDOR = 1;
            OPENSSL_DIR = "${pkgs.openssl.dev}";
            OPENSSL_LIB_DIR = "${pkgs.openssl.out}/lib";

            meta = with pkgs.lib; {
              description = "A markdown language server with Obsidian syntax support";
              homepage = "https://github.com/Feel-ix-343/markdown-oxide";
              license = licenses.cc0;
              maintainers = [ maintainers.sQVe ];
            };
          };
        }
      );
    };
}
