{
  description = "Work-related packages";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/master";
  };

  outputs =
    { self, nixpkgs }:
    let
      supportedSystems = [
        "x86_64-linux"
        "aarch64-darwin"
      ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
    in
    {
      packages = forAllSystems (
        system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
        in
        {
          # Define your work-related packages here
          awscli2 = pkgs.awscli2;
          # ... more packages
        }
      );
    };

}
