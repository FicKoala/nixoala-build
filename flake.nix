{
  description = "system configuration flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?rev=d2faa1bbca1b1e4962ce7373c5b0879e5b12cef2"; # swap to unstable if you get a system detection error
    nixfmt-rfc.url = "github:nixos/nixfmt/master";

    nix-darwin = {
      url = "github:LnL7/nix-darwin/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    zellij-flake = {
      url = "path:./zellij";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    helix = {
      url = "github:helix-editor/helix/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    markdown-oxide = {
      url = "path:./markdown-oxide";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    stables = {
      url = "path:./stables";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    readii = {
      url = "path:./readii";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # not useful until aarch64-apple-darwin has been approved: https://github.com/ghostty-org/ghostty/pull/4608
    # ghostty = {
    #   url = "github:ghostty-org/ghostty";
    # };

  };

  outputs =
    {
      self,
      nixpkgs,
      nix-darwin,
      zellij-flake,
      helix,
      markdown-oxide,
      nixfmt-rfc,
      stables,
      readii,
      ...
    }:
    let
      maxSize = 2097512;
      curlOverlay = self: super: {
        curl = super.curlMinimal.override {
          openssl = super.quictls;
          http3Support = true;
        };
      };
    in
    {
      nixpkgs.overlays = [ curlOverlay ];

      darwinConfigurations."KoalaBook" = nix-darwin.lib.darwinSystem {
        modules = [
          {
            nixpkgs.hostPlatform = {
              system = "aarch64-darwin";
            };
          }
          (
            { pkgs, ... }:
            {
              environment.systemPackages =
                with pkgs;
                [
                  # ghostty.packages.${pkgs.system}.default
                  bash-language-server
                  bat
                  bottom
                  curl
                  darwin.apple_sdk.frameworks.System
                  difftastic
                  eza
                  fd
                  fzf
                  gh
                  git-cliff
                  harper
                  helix.packages.${system}.default
                  jetbrains-mono
                  lazydocker
                  markdown-oxide.packages.${pkgs.system}.default
                  mistral-rs
                  nixd
                  nixfmt-rfc.packages.${pkgs.system}.default
                  nushell
                  ouch
                  pandoc
                  quictls
                  rip2
                  ripgrep
                  sd
                  starship
                  stow
                  taplo
                  tealdeer
                  typst
                  typst-live
                  vscode-langservers-extracted
                  yaml-language-server
                  zellij-flake.packages.${pkgs.system}.default
                  zoxide
                ]
                ++ (with stables.packages.${pkgs.system}; [
                  awscli2
                ])
                ++ (with readii.packages.${pkgs.system}; [
                  azure-cli
                  sqlcmd
                  sqls
                ]);

              nix.settings.experimental-features = [
                "nix-command"
                "flakes"
              ];
              programs.zsh.enable = true;
              system.configurationRevision = self.rev or self.dirtyRev or null;
              system.stateVersion = 4;
            }
          )
        ];
      };

      nixosConfigurations.nixoala = nixpkgs.lib.nixosSystem {
        modules = [
          {
            nixpkgs.hostPlatform = {
              system = nixpkgs.lib.mkDefault "x86_64-linux";
            };
          }
          ./configuration.nix
          (
            { pkgs, ... }:
            {
              boot.kernel.sysctl = {
                "net.core.rmem_max" = maxSize;
                "net.core.wmem_max" = maxSize;
              };

              environment.systemPackages = with pkgs; [
                bat
                difftastic
                eza
                fd
                ffmpeg-full
                fzf
                gh
                git-cliff
                harper
                helix.packages.${system}.default
                mosh
                nixfmt-rfc.packages.${pkgs.system}.default
                ripgrep
                rm-improved
                sd
                starship
                taplo
                tealdeer
                zellij-flake.packages.${pkgs.system}.default
              ];
            }
          )
        ];
      };
    };
}
